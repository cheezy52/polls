# == Schema Information
#
# Table name: responses
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  answer_choice_id :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class Response < ActiveRecord::Base
  validates :user_id, :answer_choice_id, :presence => true

  validate :respondent_has_not_already_answered_question

  validate :respondent_is_not_poll_author

  belongs_to(
    :respondent,
    :primary_key => :id,
    :foreign_key => :user_id,
    :class_name => "User"
  )

  belongs_to(
    :answer_choice,
    :primary_key => :id,
    :foreign_key => :answer_choice_id,
    :class_name => "AnswerChoice"
  )

  has_one(
    :question,
    :through => :answer_choice,
    :source => :question
  )

  def respondent_has_not_already_answered_question
    our_responses = existing_responses
    case our_responses.length
    when 1
      if our_responses.first.id != id
        errors[:response] << "Question already answered by this user"
      end
    when 0
      nil
    else
      errors[:response] << "Question already answered by this user"
    end
  end

  def existing_responses_three_queries
    question.responses
  end

  def existing_responses_two_queries
    self.class
    .joins(:answer_choice => :question)
    .where("answer_choices.id = ?", self.answer_choice_id)
    .where("questions.id = ?", AnswerChoice.where("id = ?" , self.answer_choice_id)
    .select(:question_id)
    .pluck(:question_id)).to_a
  end

  def existing_responses
    query = <<-SQL
    SELECT
      responses.*
    FROM
      responses
    JOIN
      answer_choices ON responses.answer_choice_id = answer_choices.id
    JOIN
      questions ON answer_choices.question_id = questions.id
    WHERE
      responses.user_id = ?
    AND
      questions.id =
      (SELECT DISTINCT
         answer_choices.question_id
       FROM
         answer_choices
       WHERE
         answer_choices.id = ?
       )
    SQL

    Response.find_by_sql [query, self.user_id, self.answer_choice_id]
  end

  def respondent_is_not_poll_author
    if self.class
      .joins(:question =>
        { :poll => :author }
      ).where("users.id = ?", self.user_id)
      .any?
      errors[:response] << "Author can't respond to own poll!"
      return false
    end
    true
  end

  def respondent_is_not_poll_author_chain
    #5 SQL queries - version with joins only does one
    if question.poll.author.id == respondent.id
      errors[:response] << "Author can't respond to own poll!"
    end
  end

end
