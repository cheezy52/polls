# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  user_name  :string(255)
#  created_at :datetime
#  updated_at :datetimerequire "../../db/seeds"

#

class User < ActiveRecord::Base
  validates :user_name, :presence => true, :uniqueness => :true


  has_many(
    :authored_polls,
    :primary_key => :id,
    :foreign_key => :author_id,
    :class_name => "Poll"
    )

  has_many(
    :responses,
    :primary_key => :id,
    :foreign_key => :user_id,
    :class_name => "Response"
    )

  def completed_polls
    poll_completion[0]
  end

  def uncompleted_polls
    poll_completion[1]
  end

  def poll_completion
    our_polls = Poll.joins(:questions => { :answer_choices => :responses } )
      .where("responses.user_id = ?", self.id)
      .group("polls.title")
      .count

    poll_questions = Poll.joins(:questions).group("polls.title").count

    completed_polls = []
    uncompleted_polls = []
    poll_questions.each_key do |poll_title|
      if our_polls[poll_title] == poll_questions[poll_title]
        completed_polls << poll_title
      else
        uncompleted_polls << poll_title
      end
    end
    [completed_polls, uncompleted_polls]
  end
end
