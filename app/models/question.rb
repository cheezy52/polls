# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  text       :text
#  poll_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Question < ActiveRecord::Base
  validates :text, :poll_id, :presence => :true

  belongs_to(
    :poll,
    :primary_key => :id,
    :foreign_key => :poll_id,
    :class_name => "Poll"
  )

  has_many(
    :answer_choices,
    :primary_key => :id,
    :foreign_key => :question_id,
    :class_name => "AnswerChoice",
    :dependent => :destroy
  )

  has_many(
    :responses,
    :through => :answer_choices,
    :source => :responses
  )

  def results
    self.class.joins(:answer_choices)
    .joins("LEFT OUTER JOIN responses ON answer_choices.id = responses.answer_choice_id")
    .where("questions.id = ?", self.id)
    .group("answer_choices.text")
    .count("responses.id")
  end
end
