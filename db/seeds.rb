# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

ben = User.create!(:user_name => "Ben")
kiran = User.create!(:user_name => "Kiran")
jeff = User.create!(:user_name => "Jeff")
ned = User.create!(:user_name => "Ned")

cat_names = Poll.create!(:title => "Cat Names", :author_id => ned.id)
ar_poll = Poll.create!(:title => "Active Record", :author_id => jeff.id)

cat1 = Question.create!(:text => "What is the best cat name?", :poll_id => cat_names.id)
cat2 = Question.create!(:text => "Why are cats so awesome?", :poll_id => cat_names.id)
ar1 = Question.create!(:text => "How much do you love ActiveRecord?", :poll_id => ar_poll.id)
ar2 = Question.create!(:text => "How much does raw SQL suck?", :poll_id => ar_poll.id)

cat11 = AnswerChoice.create!(:text => "Andromeda", :question_id => cat1.id)
cat12 = AnswerChoice.create!(:text => "Ajax", :question_id => cat1.id)
cat21 = AnswerChoice.create!(:text => "soft fur", :question_id => cat2.id)
cat22 = AnswerChoice.create!(:text => "purring", :question_id => cat2.id)

ar11 = AnswerChoice.create!(:text => "not much", :question_id => ar1.id)
ar12 = AnswerChoice.create!(:text => "a lot", :question_id => ar1.id)
ar21 = AnswerChoice.create!(:text => "not much", :question_id => ar2.id)
ar22 = AnswerChoice.create!(:text => "a lot", :question_id => ar2.id)

brc1 = Response.create!(:user_id => ben.id, :answer_choice_id => cat11.id)
brc2 = Response.create!(:user_id => ben.id, :answer_choice_id => cat21.id)
krc1 = Response.create!(:user_id => kiran.id, :answer_choice_id => cat12.id)
krc2 = Response.create!(:user_id => kiran.id, :answer_choice_id => cat21.id)